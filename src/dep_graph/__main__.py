"""This is the entrypoint

Pre-conditions for usage of this entrypoint:
    1. A parameter is passed (this parameter is supposed to contain the file name)
"""

import sys
from .dependency_printer import dependency_printer

try:
    FILE_PATH = sys.argv[1]
    dependency_printer(FILE_PATH)
except IndexError as index_err:
    print("You did not specified a file name as parameter")
