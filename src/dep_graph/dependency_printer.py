"""This file is intended to store functions used to display on stdout a dependency tree

Currently, there is a single function in this file

TODO: The code to open JSON file should be improved in a future version, to avoid
disabling the broad-except rule
"""
# pylint: disable=broad-except

import json
import sys
from .dependency_tree import DependencyTree


def dependency_printer(file_location: str) -> None:
    """This function prints on stdout a Python package dependency tree

    Pre-conditions for usage of this function:
        1. file_location represents a path to a valid file
    """
    try:
        with open(file_location, 'r') as file:
            json_file_content = json.load(file)
    except Exception as except_caught:
        print("An exception occurred while opening file: " + file_location + str(except_caught))
        sys.exit(1)

    dependency_tree = DependencyTree(json_file_content)

    dependency_tree.print_dependency_tree()
