"""The purpose of this file is to host class DependencyTree

No other purpose is expected for this file in the future
"""

from typing import List, Dict


class DependencyTree:
    """This class describes a Python package dependency tree.

    It provides storage capacities for a such dependency tree
    It provides also methods to process this dependency tree

    Pre-conditions for usage of this class:
        1. package_dependency parameter is of format Dict[str, List[str]]
          1.1 Where keys of the dictionary describes packages,
              and values dependencies of the package mentioned as key
        2. package_dependency represents a tree structure (e.g. it's acyclic)
        3. Each string of package_dependency is a valid Python package name
    """
    def __init__(self, package_dependency: Dict[str, List[str]] = None) -> None:
        if package_dependency is None:
            __package_dependency = {}
        self.__package_dependency = package_dependency

    def __get_package_list(self) -> List[str]:
        try:
            return list(self.__package_dependency.keys())
        except LookupError as lookup_err:
            print("Package dependencies of this object are not a dictionary: " + str(lookup_err))
        return None

    def __get_dependency_of_package(self, package: str) -> List[str]:
        try:
            return self.__package_dependency[package]
        except LookupError as lookup_err:
            print("There is no package with name: " + package + str(lookup_err))
        return None

    def print_dependency_tree(self) -> None:
        """Public method used to print a dependency tree on stdout

        Time complexity: O(n),
        where n equals to the total number of packages (including dependencies)

        Space complexity: O(1)
        """
        for package in self.__get_package_list():
            self.print_package_dependency_tree(package)

    def print_package_dependency_tree(self, package: str, depth: int = 0) -> None:
        """Public method used to print a dependency tree on stdout,
        of package "package".
        It's a recursive method.

        Arguments:
            package: String that corresponds to the name of a package
            depth: Parameter used to change the number of displayed space
            following depth in package tree

        Time complexity: O(m), where m equals to
        the total number of dependencies of package "package"

        Space complexity: O(1)
        """
        print("  " * depth, "-", package)
        for dependency in self.__get_dependency_of_package(package):
            self.print_package_dependency_tree(dependency, depth + 1)
