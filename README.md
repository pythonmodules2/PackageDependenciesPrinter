# General information


PackageDependenciesPrinter is a project that contains Python code to print to stdout a package dependency tree.

This dependency tree should be stored in a JSON file, that is passed as input.

For the sake of maintainability, it relies only on Python standard library.
The goal is to avoid relying on external providers when managing version updates.

The Gitlab CI infrastructure is checking for security issues on the code,
plus performs linting, unit testing and building.

Detailed technical information is available inside the code.


# Usage

Install the package with:

```
python3 -m pip install <package_file>
```

Use the package with:

```
python3 -m dep_graph <json_input_file>
```


# Unit test

From root project directory:

```
python3 -m unittest tests/test_complex_dependencies.py
```


# Limitations

* <b>Class structure</b>: A package dependency tree is represented as an object of type DependencyTree.
  It is enough for satisfying current needs, but to increase maintainability we would need to create other classes,
  to describe more precisely what is a dependency tree (by creating classes Package, Dependency, ...).
* <b>Storage of the resolved dependency tree</b>: Within DependencyTree objects, the resolved dependency tree is
  not stored as an object, it can just be printed to stdout. One concrete consequence is that test cases only relies on
  stdout output, this is not maintainable. <b>It needs to be fixed.</b>
* <b>Test cases</b>: Currently there is a test case to validate that the code works when input file is correct, but
  we need test cases to validate that code can detect a non-JSON file,
  a dependency tree that contains cyclic dependency, ...
* <b>Usage of the package</b>: More messages could be added to help the user to use the package, once it's installed.


## About

Made by @ol456014.
