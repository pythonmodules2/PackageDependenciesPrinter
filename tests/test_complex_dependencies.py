"""This file is intended to host complex tests.

Currently, there is only one test but more tests could be added to this file in the future
"""

from io import StringIO
from unittest import TestCase, main
from unittest.mock import patch
from os.path import dirname, join
from src.dep_graph.dependency_printer import dependency_printer


class TestComplexDependency(TestCase):
    """This test checks that the function dependency_printer works as expected

    when the input JSON file is fully valid.

    Known limitation: The test is working, but it only relies on stdout,
    so the variable "expected_stdout" is not easily readable in the code by a human

    """
    def test_stdout(self):
        """Compares the expected stdout output with the current stdout output

        Test is successful when we have exactly the expected output,
        otherwise the test is failed
        """
        expected_stdout = """ - pkg1
   - pkg2
     - pkg3
       - pkg4
     - pkg4
   - pkg3
     - pkg4
   - pkg4
 - pkg2
   - pkg3
     - pkg4
   - pkg4
 - pkg3
   - pkg4
 - pkg4
"""

        file_dir = dirname(__file__)
        input_file_dir = "test_input_files/TestComplexDependencies.json"
        absolute_input_file_dir = join(file_dir, input_file_dir)

        with patch('sys.stdout', new=StringIO()) as captured_stdout:
            dependency_printer(absolute_input_file_dir)
            self.assertEqual(captured_stdout.getvalue(), expected_stdout)

        print(captured_stdout.getvalue())


if __name__ == '__main__':
    main()
